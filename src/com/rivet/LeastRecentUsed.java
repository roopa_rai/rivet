package com.rivet;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public class LeastRecentUsed {
    static Deque<HashMap<Integer,String>> queue;   
    static HashSet<HashMap<Integer,String>> map; 
    static int size; 
      
    LeastRecentUsed(int n) 
    { 
        queue=new LinkedList<HashMap<Integer,String>>(); 
        map=new HashSet<HashMap<Integer,String>>(); 
        size=n; 
    } 
      
    public void add(HashMap<Integer,String> x) 
    { 
        if(!map.contains(x)) 
        { 
            if(queue.size()==size) 
            { 
            	HashMap<Integer,String> last=queue.removeLast(); 
                map.remove(last); 
            } 
        } 
        else
        { 
            queue.remove(x); 
        } 
        queue.push(x); 
        map.add(x); 
    } 
      
    // display contents of cache  
    public void display() {
    	queue.stream().forEach(x-> System.out.println(x));
       }
    
}
    
 