package com.rivet;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class RivetMain {

	public static void main(String[] args) throws IOException {
		LeastRecentUsed lru=new LeastRecentUsed(4);
		HashMap<Integer,String> hm=new HashMap<Integer,String>();
		hm.put(1, "test1");
		hm.put(2, "teat2");
		hm.put(3,"test3");
		hm.put(4, "teat4");
		hm.put(1,"test3->1");
		hm.put(5,"test5");
		hm.put(4, "teat4->4");
		lru.add(hm);
		lru.display();
		System.out.println("\n-------------------");
		String fileName1 = "/home/roopa/source1.txt";
		Path path1 = Paths.get(fileName1);
		String fileName2 = "/home/roopa/source2.txt";
		Path path2 = Paths.get(fileName2);
		List<String> file1 = Files.readAllLines(path1, StandardCharsets.UTF_8);
		List<String> file2 = Files.readAllLines(path2, StandardCharsets.UTF_8);
		List<String> fileResultOf1=file1.stream().filter(x-> !file2.contains(x)).collect(Collectors.toList());
		fileResultOf1.stream().forEach(x-> System.out.println(x));
		System.out.println("-------------------");
		List<String> fileResultOf2=file2.stream().filter(x-> !file1.contains(x)).collect(Collectors.toList());
		fileResultOf2.stream().forEach(x-> System.out.println(x));

	}

}
